let operation = '';
let fromDisplay = document.getElementById('display').innerText;
let memoryCurrentNumber = null;
let memoryNumber = false;

function roundToTwo(num) {
	return +(Math.round(num + "e+2")  + "e-2");
}
function gearsButton(numb) {
	if (memoryNumber) {
		display(numb);
		memoryNumber = false;
	} else {
		display(numb,true);
	}
}
function waitOperation(operation) {

	if (operation === '+') {
		resultBatton(operation);
		return true;
	}
	else if (operation === '-') {
		resultBatton(operation);
		return true;
	}
	else if (operation === '/') {
		resultBatton(operation);
		return true;
	}
	else if (operation === '*') {
		resultBatton(operation);
		return true;
	}
	 else
		return false;

}
function display(view,add = false) {
	let viewStr = view + '';


	if (fromDisplay.length >= 12) {return true;}

	if (add) {
		fromDisplay = document.getElementById('display').innerText += viewStr;
		return true;
	}
	if(viewStr.indexOf('.') !== -1) {
		viewStr = roundToTwo(viewStr);
	}
	if (viewStr.length >= 12) {
		viewStr = viewStr.substring(0,9);
		viewStr += '...';
		document.getElementById('memory').innerText = 'Слишком большое число';
		fromDisplay = document.getElementById('display').innerText = viewStr;
		fromDisplay = 0;
		return true;
	}
	fromDisplay = document.getElementById('display').innerText = viewStr;

	return true;

}

function buttonNub(id) {

	if (fromDisplay === '0') {
		document.getElementById('display').innerText = '';
	}
	switch (id) {
		case 'id1': gearsButton(1);
			break;
		case 'id2': gearsButton(2);
			break;
		case 'id3': gearsButton(3);
			break;
		case 'id4': gearsButton(4);
			break;
		case 'id5': gearsButton(5);
			break;
		case 'id6': gearsButton(6);
			break;
		case 'id7': gearsButton(7);
			break;
		case 'id8': gearsButton(8);
			break;
		case 'id9': gearsButton(9);
			break;

	}

}

function mathFunc(mathSignId) {

	let localOperation = '';
	switch (mathSignId) {
		case 'plus': localOperation = '+';
			break;
		case 'minus': localOperation = '-';
			break;
		case 'multiplication': localOperation = '*';
			break;
		case 'divide': localOperation = '/';
			break;
	}

	let localMemory = parseFloat(fromDisplay);
	if (memoryNumber) {
		display(memoryCurrentNumber);
	} else {
		if (memoryCurrentNumber === null) {
			memoryNumber = true;
			memoryCurrentNumber = localMemory;

		} else {
			if (localOperation !== operation) {
				waitOperation(operation);
				operation = localOperation;
				return true
			}
			memoryNumber = true;
			document.getElementById('memory').innerText = `${memoryCurrentNumber}${localOperation}${localMemory}`;
			if(localOperation === '+') {
				memoryCurrentNumber += localMemory;
			} else if (localOperation === '-') {
				memoryCurrentNumber -= localMemory;
			} else if (localOperation === '*') {
				memoryCurrentNumber *= localMemory;
			} else if (localOperation === '/') {
				memoryCurrentNumber /= localMemory;
			}

		}

	}
	operation = localOperation;
	display(memoryCurrentNumber);

	return true;
}

/*ФУНКЦИИ*/
function plusminusBatton() {
	if(fromDisplay.indexOf('-') === -1 && fromDisplay !== 0 && !memoryNumber) {
		display('-' + fromDisplay);
		//fromDisplay = document.getElementById('display').innerText = '-' + fromDisplay;
	} else if(!fromDisplay.indexOf('-')) {
		display(fromDisplay.substr( 1));
	}

}
function decimalBatton() {
	if(fromDisplay.indexOf('.') === -1) {
		display( '.', true);
	}
	console.log(fromDisplay)


}
function resultBatton(oper = operation) {
	switch (oper) {
		case "+":
			mathFunc('plus');
			break;
		case "*":
			mathFunc('multiplication');
			break;
		case "/":
			mathFunc('divide');
			break;
		case "-":
			mathFunc('minus');
			break;
	}
}
function delLastBatton () {
	if(!memoryNumber){
		display(fromDisplay.substring(0, fromDisplay.length - 1));
	}
}
function resetBatton() {
	fromDisplay = document.getElementById('display').innerText = '0';
	memoryCurrentNumber = null;
	document.getElementById('memory').innerText = '';
}